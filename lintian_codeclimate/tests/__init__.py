# Copyright: 2024 Cardiff University
# SPDX-License-Idenfitifer: MIT

"""Tests for lintian-codeclimate
"""

__author__ = "Duncan Macleod <macleoddm@cardiff.ac.uk>"
